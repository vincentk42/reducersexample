import React, {useEffect} from 'react';

import {createAppContainer} from '@react-navigation/native';
import {createStackNavigator} from 'react-navigation-stack';
import Firstpage from './src/pages/Firstpage';
import Secondpage from './src/pages/Secondpage';
import Thirdpage from './src/pages/Thirdpage';
import {Provider} from './src/context/MathContext';

const Appnavigator = createStackNavigator({
  Firstpage: Firstpage,
  Secondpage: Secondpage,
  Thirdpage: Thirdpage,
});

const Appcontainer = createAppContainer(Appnavigator);

const App = () => {
  return (
    <Provider>
      <Appcontainer />
    </Provider>
  );
};

export default App;

/*
/*


export default () => {
  return (
    <Provider>
      <AppContainer />
    </Provider>
  );
};



export default () => {
  return (
    <>
      <GlobalState>
        <Appcontainer />
      </GlobalState>
    </>
  );
};
import React from 'react';

import {createAppContainer} from '@react-navigation/native';
import {createStackNavigator} from 'react-navigation-stack';
import Firstpage from './src/pages/Firstpage';
import Secondpage from './src/pages/Secondpage';
import Thirdpage from './src/pages/Thirdpage';
import {Provider} from './src/context/MathContext';

const Appnavigator = createStackNavigator({
  Firstpage: Firstpage,
  Secondpage: Secondpage,
  Thirdpage: Thirdpage,
});

const Appcontainer = createAppContainer(Appnavigator);

// const App: () => React$Node = () => {
//   return (
//     <>
//       <Provider>
//         <Appcontainer />
//       </Provider>
//     </>
//   );
// };

// export default App;

export default () => {
  return (
    <Provider>
      <Appcontainer />
    </Provider>
  )
}

*/
