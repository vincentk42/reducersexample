import React, {useContext, useEffect} from 'react';
import {View, Text} from 'react-native';
import {Context} from '../context/MathContext';

const Secondpage: navigation => React$Node = navigation => {
  const {state} = useContext(Context);
  // const {updatedState} = useContext(MainContext);
  // const everything = useContext(MainContext);
  useEffect(() => {
    console.log('hey look', state);
  });
  return (
    <>
      <View>
        <Text>Secondpage</Text>
        <Text>show me the new number {state.startingNumber}</Text>
      </View>
    </>
  );
};

export default Secondpage;
