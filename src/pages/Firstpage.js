import React, {useContext, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import MainContext from '../context/MainContext';
import {Context} from '../context/MathContext';

const Firstpage: navigation => React$Node = navigation => {
  // const {addDigit, subtractDigit} = useContext(MainContext);
  const {state, addDigit, subtractDigit} = useContext(Context);
  useEffect(() => {
    console.log('show me state: ', state)
  },[])
  return (
    <>
      <View>
        <Text>Hello world</Text>
        <Button
          title={'go to third page'}
          onPress={() => navigation.navigation.navigate('Thirdpage')}
        />
        <Text>placeholder</Text>
        <Button
          title={'Go to second page'}
          onPress={() => navigation.navigation.navigate('Secondpage')}
        />
        <Text>Something to add</Text>
        <Button
          title={'adding'}
          onPress={() => {
            console.log('fuck');
            addDigit();
            console.log('here is the final state: ', state)
          }}
        />
        <Text>filler</Text>
        <Button
          title={'Subtracting'}
          onPress={() => {
            subtractDigit();
            console.log('pressinge subtract', state);
          }}
        />
      </View>
    </>
  );
};

export default Firstpage;
