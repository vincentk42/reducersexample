export const ADD_NUMBER = 'ADD_NUMBER';
export const SUBTRACT_NUMBER = 'SUBTRACT_NUMBER';

const addDigit = (state, payload) => {
  const updatedStateValue = state.count;
  console.log(' redudcer inside addDigig: ', state, payload)
  return {
    count: updatedStateValue + 1,
  };
};

const subtractDigit = (state, payload) => {
  const updatedStateValue = state.count;

  return {
    count: updatedStateValue - payload,
  };
};

export const mainReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_NUMBER':
      console.log('export inside maindreducer: ', state, action);
      return addDigit(state, 1);
    case 'SUBTRACT_NUMBER':
      return subtractDigit(state, 1);
    default:
      return state;
  }
};
