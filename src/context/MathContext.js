import createDataContext from './createDataContext';

const mathReducer = (state, action) => {
  switch (action.type) {
    case 'add_number':
      console.log('whats the current state: ', state);
      return {
        ...state,
        startingNumber: state.startingNumber + action.payload,
      };
    case 'subtract_number':
      return {
        ...state,
        startingNumber: state.startingNumber - action.payload,
      };
  }
};
const addDigit = dispatch => {
  return () => {
    dispatch({
      type: 'add_number',
      payload: 1,
    });
  };
};

const subtractDigit = dispatch => {
  return () => {
    dispatch({
      type: 'subtract_number',
      payload: 1,
    });
  };
};

export const {Context, Provider} = createDataContext(
  mathReducer,
  {
    addDigit,
    subtractDigit,
  },

  {
    startingNumber: 0,
  },
);
