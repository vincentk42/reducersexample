import React from 'react';

const LocationContext = React.createContext();
export const LocationProvider = ({children}) => {
  return (
    <LocationContext.Provider value={55}>{children}</LocationContext.Provider>
  );
};

