import React, {useReducer, useEffect} from 'react';
import {mainReducer, ADD_NUMBER, SUBTRACT_NUMBER} from './reducers';
import MainContext from './MainContext';

// const MainContext = React.createContext({latitude: 333.3333, longitude: 1.1});
/*userReducer is calleed so because it follows that same API as .reduce*/
let initialState = {
  count: 0,
  latitude: 0,
  longitude: 0,
  stupidmessage: '',
};
let longitude;
let latitude;
const GlobalState: props => React$Node = props => {
  const [state, dispatch] = useReducer(mainReducer, initialState);

  const addDigit = () => {
    console.log('Global State')
    dispatch({type: ADD_NUMBER, payload: 1});
  };

  const subtractDigit = () => {
    dispatch({type: SUBTRACT_NUMBER, payload: 1});
  };
 

  useEffect(() => {
    // console.log('show me the state inside useEffect', state);
    latitude = 1.12344;
    initialState.longitude = 4333333234223;
    console.log('show me state: ', state);
  }, []);
  return (
    <MainContext.Provider
      value={{
        initialState: initialState,
        addDigit: addDigit,
        subtractDigit: subtractDigit,
        updatedState: state.count,
        latitude: initialState.latitude,
        longitude: initialState.longitude,
      }}>
      {props.children}
    </MainContext.Provider>
  );
};

export default GlobalState;
